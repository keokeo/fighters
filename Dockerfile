FROM node:8.9.3-alpine

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY . /usr/src/app/
RUN npm install --no-optional

# Build app
RUN cp /usr/src/app/.env.template /usr/src/app/.env
RUN npm run build

EXPOSE 3000

# start command
CMD [ "npm", "start" ]
