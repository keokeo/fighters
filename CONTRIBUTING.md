#  CONTRIBUTING

### 1. Prerequisites

- We use Docker to start all required services (MongoDB, Redis, NGINX...) so you have to install [Docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install/) on your machine first.
- Install [mkcert](https://github.com/FiloSottile/mkcert) to generate a valid TLS certificate for local server.
- For frontend, install [NodeJS](https://nodejs.org/en/) >= 11.0 and [yarn](https://yarnpkg.com/en/) or npm (not recommended) as package management.
- For backend, install Go >= 1.11 (we use go module as package management).
- Install [realize](https://github.com/oxequa/realize) for Go hot-reloading when develop `executioner` (backend) service (optional).

### 2. Generate TLS certificate

```shell
$ cd ~/cf/certs
$ mkcert -install   # Install local CA to system trust store.
$ mkcert codefighter.local
# After finished, you will see 2 files: codefighter.local.pem and codefighter.local-key.pem
# Remember the path to these two files, we will need it later.
```

### 3. Start frontend and all required services

```shell
# Clone fighters (frontend service) code to your local
$ cd ~/cf
$ git clone https://gitlab.com/fighters-solution/fighters
$ cd fighters

# Download dependencies
$ yarn   # or npm install

# Copy two generated files at step 2 to deployment folder
$ cd deployment   # ~/cf/fighters/deployment
$ cp ~/cf/certs/* .

# Edit docker-compose.yml config
$ cp docker-compose.yml.template docker-compose.yml
# You can edit:
#   - Line #14: Change path to store MongoDB data on your local machine.
#   - Line #27: Change path to store Redis data on your local machine.
#   - Line #42-#60: Can be changed to configure frontend server. Make sure to edit line #51.

# Start services
$ docker-compose up
```

### 4. Start backend service

Note: Remember to install Go >= 1.11. We're using go module as dependencies manangement and it's only available on Go >= 1.11.

```shell
# Clone executioner (backend service) code to your local
$ cd ~/cf
$ git clone https://gitlab.com/fighters-solution/executioner
$ cd executioner

# Edit configurations
$ cd configs   # ~/cf/executioner/configs
$ cp config.yml.template config.dev.yml
# You can edit:
#   - Line #3 (server.address): Make sure to keep it empty or "0.0.0.0".
#   - Line #14 (server.security.enable_email_confirm): Change to false to disable sending email confirmation.
#   - Line #16 (server.security.session.secret): Put your secret here, length = 32.

$ cd ..   # ~/cf/executioner

# If you're using Linux and have make tool installed:
$ make vendor   # Download dependecies 
$ make run   # Start backend service

# If you're using Windows or don't have make tool installed:
$ GO111MODULE=on go mod vendor   # Download dependencies
$ go run main.go

# If you already installed realize tool for hot-reloading then 
# instead of running `make run` or `go run main.go` you can run this command
$ realize start
# You can see hot-reload dashboard at http://codefighter.local:10081 or http://localhost:10081
```

Open browser and head to: https://codefighter.local:10443, and everything should be ready.

### 5. Database for dev

Ping [Vu To](mailto:tntvu@tma.com.vn) or [Quy Le](mailto:lnquy@tma.com.vn) if you need sample data for local development.

