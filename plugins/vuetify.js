import '@fortawesome/fontawesome-free/css/all.css'
import Vue from 'vue'
import Vuetify from 'vuetify'
import CFLogoIcon from '@/components/CFLogoIcon.vue'

Vue.use(Vuetify, {
  iconfont: 'md' || 'fa',
  icons: {
    'cf-icon': {
      component: CFLogoIcon
    }
  }
})
